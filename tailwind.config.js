module.exports = {
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  theme: {
    extend: {
      colors: {
        'cyan-blue': '#EBF4FF',
        'light-cyan': '#E6FFFA',
        'dark-cyan': '#319795',
        'grayish-blue': '#2D3748',
        'light-grayish-blue': '#718096',
        grayish: '#F7FAFC',
        blue: '#3182CE',
        riptide: '#81E6D9',
        finn: '#4A5568',
      },
      boxShadow: {
        't-xl': '-2px -4px 25px -7px rgba(0, 0, 0, 0.3)',
      },
    },
  },
  plugins: [],
}
